const Course = require("../models/Course.js");
const auth = require("../auth.js");

/* 
    Steps:
    1. Create a new Course object using the mongoose model and information from the request of the user.
    2. Save the course to the database
*/
module.exports.addCourse = (request, response) => {
    let newCourse = new Course({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        slots: request.body.slots,
    });

    let token = request.headers.authorization;
    const userData = auth.decode(token);
    console.log(userData);

    if (!userData.isAdmin) {
        return response.send(`You are not permitted to add courses`);
    } else {
        return newCourse
            .save()
            .then((result) => {
                console.log(result);
                response.send(true);
            })
            .catch((error) => {
                console.log(error);
                response.send(false);
            });
    }
};

// Retrieve all active courses
module.exports.getAllActive = (request, response) => {
    return Course.find({ isActive: true })
        .then((result) => {
            response.send(result);
        })
        .catch((err) => {
            response.send(err);
        });
};

// Retrieving a specific course
module.exports.getCourse = (request, response) => {
    const courseId = request.params.courseId;

    return Course.findById(courseId)
        .then((result) => {
            response.send(result);
        })
        .catch((err) => {
            response.send(err);
        });
};

// update a course

module.exports.updateCourse = (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.decode(token);
    console.log(userData);

    let updatedCourse = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        slots: request.body.slots,
    };

    const courseId = request.params.courseId;

    if (userData.isAdmin) {
        return Course.findByIdAndUpdate(courseId, updatedCourse, { new: true })
            .then((result) => {
                response.send(result);
            })
            .catch((err) => {
                response.send(err);
            });
    } else {
        return response.send(`You don't have access to this page`);
    }
};

/* S40 - ACTIVITY */

module.exports.archiveCourse = (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.decode(token);
    console.log(userData);

    const courseId = request.params.courseId;
    // const newStatus = request.body.isActive;

    const newStatus = {
        isActive: request.body.isActive,
    };

    if (userData.isAdmin) {
        // return Course.findByIdAndUpdate(courseId, {isActive:newStatus}, {new:true})
        return Course.findByIdAndUpdate(courseId, newStatus, { new: true })
            .then((result) => {
                response.send(result.isActive);
            })
            .catch((err) => {
                response.send(err);
            });
    } else {
        return response.send(`You don't have access to this page`);
    }
};

/* Retrieve all course including those inactive course */

/* 
    Steps:
        1. Retrieve all the courses (active/inactive)
        2. Verify the role of the current user(Admin to continue)
*/

module.exports.getAllCourses = (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.decode(token);
    console.log(userData);

    if (!userData.isAdmin) {
        return response.send(`Sorry, you don't have access to this page`);
    } else {
        return Course.find({})
        .then((result) => {
            response.send(result);
        })
        .catch((err) => {
            console.log(err);
            response.send(err);
        });
    }
};
