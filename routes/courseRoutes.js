const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Adding course
router.post("/", auth.verify, courseController.addCourse);

// All active courses
router.get("/allActiveCourses", courseController.getAllActive);

// retrieve all courses
router.get("/allCourses", auth.verify, courseController.getAllCourses);

/* WITH PARAMS */
// specific course
router.get("/:courseId", courseController.getCourse);

// update specifc course
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// archive course - S40 ACTIVITY
router.patch("/:courseId/archive", auth.verify, courseController.archiveCourse);

module.exports = router;
