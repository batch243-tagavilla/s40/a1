const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/checkEmail", userController.checkEmailExists);

router.post("/register", userController.checkEmailExists, userController.registerUser);

router.post("/login", userController.loginUser);

router.post("/details", auth.verify, userController.getProfile);

router.get("/profile", userController.profileDetails);

// Update role
router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

// Enrollment
router.post("/enroll/:courseId", auth.verify, userController.enroll);


module.exports = router;
